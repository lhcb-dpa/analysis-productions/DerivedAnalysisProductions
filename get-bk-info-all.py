#!/usr/bin/env python
import json
import subprocess
from pathlib import Path
metadata = json.loads(Path("metadata.json").read_text())
for info in metadata:
    tid = info["tid"]
    # assert len(info["request_id"]) == 1, info
    # tids = {x.split("/")[5] for x in info["input_lfns"]}
    # assert len(tids) == 1
    # tid = tids.pop()
    metadata_path = Path("metdata") / f"{tid}.json"
    metadata_path.parent.mkdir(exist_ok=True)
    if not metadata_path.is_file():
        print("Getting info for", tid)
        x = json.loads(subprocess.check_output(["lb-dirac", "./get-bk-info.py", tid], text=True).strip().split("\n")[-1])
        metadata_path.write_text(json.dumps(x))
print("Done getting bk metadata")
