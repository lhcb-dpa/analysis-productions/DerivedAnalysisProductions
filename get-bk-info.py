#!/usr/bin/env python
import argparse
import json
import DIRAC
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise

if __name__ == "__main__":
    DIRAC.initialize()
    from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
    from LHCbDIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
    parser = argparse.ArgumentParser()
    parser.add_argument("tid", type=int)
    args = parser.parse_args()

    print(json.dumps(returnValueOrRaise(BookkeepingClient().getProductionInformation(args.tid))))
    # TransformationClient().getBookkeepingQuery(181798)["Value"]
