import json
import hashlib
from datetime import datetime, timedelta
from pathlib import Path
from pprint import pprint

import DIRAC
DIRAC.initialize()
from DIRAC.Core.Utilities.File import generateGuid
from LHCbDIRAC.Core.Utilities.BookkeepingJobInfo import BookkeepingJobInfo

metadata = json.loads(Path("metadata.json").read_text())
info = metadata[50]

version = info["wildcards"]["version"]
assert len(info["output_files"]) == 1
output_fn = Path(f"/eos/lhcb/wg/dpa/wp2/recompress-{version}/") / info["output_files"]["root_file"]

start = datetime.utcnow() - timedelta(minutes=5)
end = datetime.utcnow()
parameters = {
    "configName": {"mc": "MC", "lhcb": "LHCb"}[info["common_tags"]["config"]],
    # "BKCondition"
    "eventType": info["common_tags"]["eventtype"],
}
if parameters["configName"] == "MC":
    parameters["configVersion"] = info["common_tags"]["datatype"]
elif parameters["configName"] == "LHCb":
    parameters["configVersion"] = f"Collision{info['common_tags']['datatype']}"
else:
    raise NotImplementedError("TODO")
n_cpus = 1
transform_id = 0
lbconda_env_name = "default"
lbconda_env_version = "2023-04-26_20-20"
output_filesize = output_fn.stat().st_size
merge_id = "00000001"
filetype = "RD_LBTOL0LL_DVNTUPLE.ROOT"
output_lfn = f"/lhcb/{parameters['configName']}/{parameters['configVersion']}/{filetype}/{transform_id:08d}/0000/{transform_id:08d}_{merge_id}_1.{filetype.lower()}"
output_md5 = hashlib.md5(output_fn.read_bytes()).hexdigest()
output_guid = generateGuid(output_md5, "MD5")

job_info = BookkeepingJobInfo(
    ConfigName=parameters["configName"],
    ConfigVersion=parameters["configVersion"],
    Date=end.strftime("%Y-%m-%d"),
    Time=end.strftime("%H:%M:%S"),
)
if simulation_condition := parameters.get("BKCondition"):
    job_info.simulation_condition = simulation_condition
job_info.typed_parameters = BookkeepingJobInfo.TypedParameters(
    CPUTIME=f"{(end - start).total_seconds():.0f}",
    ExecTime=f"{(end - start).total_seconds() * n_cpus:.0f}",
    NumberOfProcessors=f"{n_cpus}",
    Production=f"{transform_id}",
    Name=f"{transform_id:08d}_{merge_id}_1",
    JobStart=start.strftime("%Y-%m-%d %H:%M"),
    JobEnd=end.strftime("%Y-%m-%d %H:%M"),
    Location=DIRAC.siteName(),
    ProgramName=f"lb-conda {lbconda_env_name}",
    ProgramVersion=lbconda_env_version,
    StepID="1",
    NumberOfEvents=f"{len(info['input_lfns'])}",
)
job_info.input_files = info["input_lfns"]
job_info.output_files = [
    BookkeepingJobInfo.OutputFile(
        Name=output_lfn,
        TypeName=f"{filetype}",
        TypeVersion="1",
        EventTypeId=parameters["eventType"],
        EventStat=f"{len(info['input_lfns'])}",
        FileSize=output_filesize,
        CreationDate=end.strftime("%Y-%m-%d %H:%M:%S"),
        MD5Sum=output_md5,
        Guid=output_guid,
    ),
]

pprint(info)

print(job_info.to_xml().decode())

print(f"configname = {job_info.ConfigName!r}")
print(f"configversion = {job_info.ConfigVersion!r}")
eventtypes = {o.EventTypeId for o in job_info.output_files}
assert len(eventtypes) == 1
print(f"eventtype = {eventtypes.pop()!r}")
